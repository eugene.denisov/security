package com.forex.ForexPredicator.repository;

import com.forex.ForexPredicator.domain.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<Role, String> {

    Role findByRole(String role);
}
